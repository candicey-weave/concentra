plugins {
    kotlin("jvm") version "1.9.23"
    id("com.gitlab.candicey.stellar") version "0.2.1"
    id("net.weavemc.gradle") version "1.0.0-PRE"
    application
}

val projectName: String by project
val projectGroup: String by project
val projectVersion: String by project
val gitlabProjectId: String by project

group = projectGroup
version = projectVersion

weave {
    configure {
        name = "Concentra"
        modId = "concentra"
        entryPoints = listOf("$projectGroup.ConcentraMain")
        mixinConfigs = listOf()
        mcpMappings()
    }
    version("1.8.9")
}

stellar {
    relocate {
    }

    dependencies {
        zenithCore version "2.0.1"
        zenithCoreVersionedApi version "v1_8"
        zenithLoader version "0.3.5"
        altera version "0.2.2"
    }
}

repositories {
    mavenCentral()
    mavenLocal()
    maven("https://repo.weavemc.dev/releases")
    maven("https://repo.polyfrost.cc/releases")

    maven("https://gitlab.com/api/v4/projects/50863327/packages/maven")
}

dependencies {
    testImplementation(kotlin("test"))
    implementation("net.weavemc:loader:1.0.0-b.2")
    implementation("net.weavemc:internals:1.0.0-b.2")
    implementation("net.weavemc.api:common:1.0.0-b.2")

    implementation("cc.polyfrost:oneconfig-1.8.9-forge:0.2.2-alpha201:full")
}

tasks.test {
    useJUnitPlatform()
}

kotlin {
    jvmToolchain(8)
}

application {
    mainClass.set("MainKt")
}