package com.gitlab.candicey.concentra

import cc.polyfrost.oneconfig.internal.OneConfig
import com.gitlab.candicey.altera.Altera
import com.gitlab.candicey.altera.alteration.TransformManager
import com.gitlab.candicey.altera.simpleBytesProvider
import com.gitlab.candicey.concentra.helper.LwjglManagerHelper
import com.gitlab.candicey.concentra.hook.FrameBufferHook
import com.gitlab.candicey.concentra.hook.RawInputEventHook
import com.gitlab.candicey.concentra.listener.InitializationEventForwarder
import com.gitlab.candicey.concentra.listener.KeyInputEventForwarder
import com.gitlab.candicey.concentra.listener.MouseInputEventForwarder
import com.gitlab.candicey.concentra.listener.RenderGameOverlayEventForwarder
import com.gitlab.candicey.concentra.transform.fileeditor.MixinConfigFileEditor
import com.gitlab.candicey.concentra.transform.transformer.*
import com.gitlab.candicey.zenithcore.extension.registerHook
import com.gitlab.candicey.zenithloader.ZenithLoader
import com.gitlab.candicey.zenithloader.dependency.readConfig
import com.gitlab.candicey.zenithloader.registerMixinConfigs
import net.weavemc.api.ModInitializer
import net.weavemc.api.event.EventBus
import net.weavemc.loader.InjectionHandler
import java.lang.instrument.Instrumentation

class ConcentraMain : ModInitializer {
    @Suppress("OVERRIDE_DEPRECATION")
    override fun preInit(inst: Instrumentation) {
        ZenithLoader.loadDependencies(
            readConfig("concentra"),
            inst = inst,
        )

        val oneConfigVersion = "0.2.2-alpha201"
        val oneConfigJarUrl = "https://repo.polyfrost.org/releases/cc/polyfrost/oneconfig-1.8.9-forge/0.2.2-alpha201/oneconfig-1.8.9-forge-0.2.2-alpha201-full.jar"
        val oneConfigJarSha256 = "6e6f6a0b5f02b8ba91f9acdf0cc2e5c6b790e2ef75991b87f090dca47ba8bdb7"
        val versionDeterminerClass = "cc/polyfrost/oneconfig/Concentra\$VersionIndicator"

        val transformerManager = TransformManager(
            transformers = mutableListOf(
                OneConfigTransformer,
                PlatformCommandManagerImplTransformer,
                HudCoreTransformer,
                GLPlatformImplTransformer,
                LwjglManagerImplTransformer,
                MinecraftMixinTransformer,
            ),

            filesEditor = mutableListOf(
//                UniversalCraftFileEditor,
                MixinConfigFileEditor,
            ),

            version = oneConfigVersion,

            versionDeterminerClassName = versionDeterminerClass,
        )

        val dependency = Altera.alter(
            instrumentation = inst,
            groupId = "com.gitlab.candicey.concentra.oneconfig",
            name = "OneConfig",
            bytesProvider = simpleBytesProvider(
                url = oneConfigJarUrl,
                sha256 = oneConfigJarSha256,
                groupId = "cc.polyfrost.oneconfig",
                name = "OneConfig",
                version = oneConfigVersion,
            ),
            namespace = "mcp-srg",
            transformManager = transformerManager,
            bootstrapClass = null,
        )

        LwjglManagerHelper.initStatic()

        dependency.file.registerMixinConfigs(
            namespace = "mcp-srg",
            modId = "oneconfig",
            mixinConfigs = listOf(
                "mixins.oneconfig.json",
            )
        )

        arrayOf(
            InitializationEventForwarder,
            MouseInputEventForwarder,
            KeyInputEventForwarder,
            RenderGameOverlayEventForwarder,
        ).forEach(EventBus::subscribe)

        arrayOf(
            FrameBufferHook,
            RawInputEventHook,
        ).forEach(InjectionHandler::registerHook)

        println("Initialising OneConfig...")
        OneConfig.INSTANCE.init()
        println("OneConfig initialised.")
    }
}