package com.gitlab.candicey.concentra

import com.gitlab.candicey.zenithcore.HOME
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import java.io.File

val CONCENTRA_VERSION: String = "@@CONCENTRA_VERSION@@"

val LOGGER: Logger by lazy { LogManager.getLogger("Concentra") }

val configDirectory: File by lazy { File(HOME, ".weave/Concentra").also(File::mkdirs) }