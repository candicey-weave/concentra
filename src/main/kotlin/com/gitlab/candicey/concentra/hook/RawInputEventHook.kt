/*
 * This file is part of Concentra.
 * Concentra - Next Generation Config Library for Minecraft: Java Edition
 * Copyright (C) 2023 Candicey.
 *   <https://gitlab.com/Candicey>
 *
 * Modified and extended from the original OneConfig project by Polyfrost.
 *   Original project: OneConfig - Next Generation Config Library for Minecraft: Java Edition
 *   Copyright (C) 2021~2023 Polyfrost.
 *   <https://polyfrost.cc> <https://github.com/Polyfrost/>
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *   Concentra is licensed under the terms of the GNU General Public License version 3.0
 *   as published by the Free Software Foundation, and may be subject to additional terms specified by Candicey.
 *   You should have received a copy of the GNU General Public License version 3.0
 *   along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.gitlab.candicey.concentra.hook

import cc.polyfrost.oneconfig.events.EventManager
import cc.polyfrost.oneconfig.events.event.RawMouseEvent
import net.weavemc.api.Hook
import net.weavemc.internals.asm
import net.weavemc.internals.internalNameOf
import net.weavemc.internals.named
import org.lwjgl.input.Keyboard
import org.lwjgl.input.Mouse
import org.objectweb.asm.Opcodes
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.MethodInsnNode

/**
 * @see [net.minecraft.client.gui.GuiScreen.handleInput]
 */
object RawInputEventHook : Hook("net/minecraft/client/gui/GuiScreen") {
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        val methodNode = node.methods.named("handleInput")
        val insnList = methodNode.instructions
        val invokeVirtualInsnNode = insnList.filterIsInstance<MethodInsnNode>().filter { it.opcode == Opcodes.INVOKEVIRTUAL }

        val handleMouseInsnNode = invokeVirtualInsnNode.find { it.name == "handleMouseInput" }
        val handleKeyboardInsnNode = invokeVirtualInsnNode.find { it.name == "handleKeyboardInput" }

        insnList.insertBefore(handleMouseInsnNode, asm {
            invokestatic(internalNameOf<RawInputEventHook>(), "onHandleMouseInput", "()V")
        })

        insnList.insertBefore(handleKeyboardInsnNode, asm {
            invokestatic(internalNameOf<RawInputEventHook>(), "onHandleKeyboardInput", "()V")
        })
    }

    @JvmStatic
    fun onHandleMouseInput() {
        EventManager.INSTANCE.post(RawMouseEvent(Mouse.getEventButton(), if (Mouse.getEventButtonState()) 1 else 0))
    }

    @JvmStatic
    fun onHandleKeyboardInput() {
        val state = if (Keyboard.getEventKeyState()) {
            if (Keyboard.isRepeatEvent()) {
                2
            } else {
                1
            }
        } else {
            0
        }

        EventManager.INSTANCE.post(RawMouseEvent(Keyboard.getEventKey(), state))
    }
}