package com.gitlab.candicey.concentra.hook

import com.gitlab.candicey.zenithcore.extension.addFieldSafe
import net.weavemc.api.Hook
import org.objectweb.asm.Opcodes
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.FieldNode

/**
 * @see [net.minecraft.client.shader.Framebuffer]
 */
object FrameBufferHook : Hook("net/minecraft/client/shader/Framebuffer") {
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        node.fields.addFieldSafe(
            FieldNode(
                Opcodes.ACC_PUBLIC,
                "concentraStencilEnabled",
                "Z",
                null,
                false
            )
        )
    }
}