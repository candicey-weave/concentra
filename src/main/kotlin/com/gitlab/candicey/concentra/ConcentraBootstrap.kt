package com.gitlab.candicey.concentra

import com.gitlab.candicey.zenithcore.util.PropertyDelegate
import java.lang.instrument.Instrumentation
import java.util.function.Consumer

class ConcentraBootstrap : Consumer<Instrumentation?> {
    var bootstrapped: Boolean by PropertyDelegate(
        property = "concentra.bootstrapped",
        defaultValue = { false }
    )

    override fun accept(inst: Instrumentation?) {

        if (bootstrapped) {
            return
        }

        require(inst != null) { "Instrumentation is null" }

        ConcentraMain().preInit(inst)

        bootstrapped = true
    }
}