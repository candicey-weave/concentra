package com.gitlab.candicey.concentra

fun info(message: String) = LOGGER.info("[Concentra] $message")

fun warn(message: String) = LOGGER.warn("[Concentra] $message")

fun debug(message: String) = LOGGER.debug("[Concentra] $message")
