package com.gitlab.candicey.concentra.wrapper

import com.gitlab.candicey.concentra.warn
import com.gitlab.candicey.zenithcore.extension.subscribeEventListener
import com.gitlab.candicey.zenithcore.util.reflectMethod
import com.gitlab.candicey.zenithcore.versioned.v1_8.event.ChatSentEvent
import com.gitlab.candicey.zenithcore.versioned.v1_8.mc
import net.minecraft.command.CommandBase
import net.minecraft.command.ICommand
import net.minecraft.command.ICommandSender
import net.minecraft.util.BlockPos
import net.weavemc.api.event.SubscribeEvent

object PlatformCommandManagerImplWrapper {
    private val commands = mutableListOf<ICommand>()

    init {
        subscribeEventListener()
    }

    fun registerCommand(unmappedICommandInstance: Any) {
//        info("Registering command $unmappedICommandInstance")
        warn("Registering command is disabled")

        val unmappedClass = unmappedICommandInstance::class.java
        /*
        * "func_71517_b" -> "getCommandName"
        * "func_71518_a" -> "getCommandUsage"
        * "func_71515_b" -> "processCommand"
        * "func_71514_a" -> "getCommandAliases"
        * "func_82362_a" -> "getRequiredPermissionLevel"
        * "func_180525_a" -> "addTabCompletionOptions"
        */
        val getMethod = { name: String -> reflectMethod(unmappedClass, name) }
        val getCommandName = getMethod("func_71517_b")
        val getCommandUsage = getMethod("func_71518_a")
        val processCommand = getMethod("func_71515_b")
        val getCommandAliases = getMethod("func_71514_a")
        val getRequiredPermissionLevel = getMethod("func_82362_a")
        val addTabCompletionOptions = getMethod("func_180525_a")

        commands += object : CommandBase() {
            override fun getCommandName(): String =
                getCommandName(unmappedICommandInstance) as String

            override fun getCommandUsage(sender: ICommandSender?): String =
                getCommandUsage(unmappedICommandInstance, sender) as String

            override fun processCommand(sender: ICommandSender?, args: Array<out String>?) =
                processCommand(unmappedICommandInstance, sender, args) as Unit

            override fun getCommandAliases(): MutableList<String> =
                getCommandAliases(unmappedICommandInstance) as MutableList<String>

            override fun getRequiredPermissionLevel(): Int =
                getRequiredPermissionLevel(unmappedICommandInstance) as Int

            override fun addTabCompletionOptions(sender: ICommandSender?, args: Array<out String>?, pos: BlockPos?): MutableList<String> =
                addTabCompletionOptions(unmappedICommandInstance, sender, args, pos) as MutableList<String>
        }
    }

    @SubscribeEvent
    fun onChatSent(event: ChatSentEvent) {
        val message = event.message
        val args = message.split("\\s+".toRegex()).drop(1).toTypedArray()
        val commandName = if (args.isEmpty()) return else args[0].substring(1)
        val command = commands.firstOrNull { it.commandName == commandName } ?: return

        val commandArgs = args.drop(1).toTypedArray()
        val commandSender = mc.thePlayer

        command.processCommand(commandSender, commandArgs)
    }
}