package com.gitlab.candicey.concentra.listener

import cc.polyfrost.oneconfig.events.EventManager
import cc.polyfrost.oneconfig.events.event.HudRenderEvent
import cc.polyfrost.oneconfig.libs.universal.UMatrixStack
import com.gitlab.candicey.zenithcore.versioned.v1_8.event.RenderGameOverlayEvent
import net.weavemc.api.event.SubscribeEvent

object RenderGameOverlayEventForwarder {
    @SubscribeEvent
    fun onRenderGameOverlayPreEvent(event: RenderGameOverlayEvent.Post) {
        val new = HudRenderEvent(UMatrixStack(), event.partialTicks)

        EventManager.INSTANCE.post(new)
    }
}