package com.gitlab.candicey.concentra.listener

import cc.polyfrost.oneconfig.events.EventManager
import cc.polyfrost.oneconfig.events.event.MouseInputEvent
import com.gitlab.candicey.zenithcore.versioned.v1_8.event.MouseEvent
import net.weavemc.api.event.SubscribeEvent

object MouseInputEventForwarder {
    @SubscribeEvent
    fun onMouseEvent(event: MouseEvent) {
        val new = MouseInputEvent(event.button)

        EventManager.INSTANCE.post(new)
    }
}