package com.gitlab.candicey.concentra.listener

import cc.polyfrost.oneconfig.events.EventManager
import cc.polyfrost.oneconfig.events.event.InitializationEvent
import com.gitlab.candicey.zenithcore.versioned.v1_8.event.StartGameEvent
import net.weavemc.api.event.SubscribeEvent

object InitializationEventForwarder {
    @SubscribeEvent
    fun onInitializationEvent(event: StartGameEvent.Post) {
        val new = InitializationEvent()

        EventManager.INSTANCE.post(new)
    }
}