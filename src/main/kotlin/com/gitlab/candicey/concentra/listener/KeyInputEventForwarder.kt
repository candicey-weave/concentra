package com.gitlab.candicey.concentra.listener

import cc.polyfrost.oneconfig.events.EventManager
import cc.polyfrost.oneconfig.events.event.KeyInputEvent
import com.gitlab.candicey.zenithcore.versioned.v1_8.event.KeyboardEvent
import net.weavemc.api.event.SubscribeEvent

object KeyInputEventForwarder {
    @SubscribeEvent
    fun onKeyboardEvent(event: KeyboardEvent) {
        val new = KeyInputEvent()

        EventManager.INSTANCE.post(new)
    }
}