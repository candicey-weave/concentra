package com.gitlab.candicey.concentra.extension

import cc.polyfrost.oneconfig.events.EventManager
import com.gitlab.candicey.zenithcore.util.weave.getSingleton
import com.gitlab.candicey.zenithcore.util.weave.internalNameOf
import net.weavemc.internals.InsnBuilder

fun InsnBuilder.callOneConfigEvent() {
    getSingleton<EventManager>()
    invokevirtual(internalNameOf<EventManager>(), "post", "()V")
}