package com.gitlab.candicey.concentra.extension

import com.gitlab.candicey.zenithcore.util.ShadowField
import net.minecraft.client.renderer.OpenGlHelper
import net.minecraft.client.shader.Framebuffer

var Framebuffer.concentraStencilEnabled: Boolean by ShadowField()

fun Framebuffer.enableStencil(): Boolean {
    return if (!OpenGlHelper.isFramebufferEnabled()) {
        false
    } else {
        concentraStencilEnabled = true
        createBindFramebuffer(framebufferWidth, framebufferHeight)
        true
    }
}