package com.gitlab.candicey.concentra.extension

import org.objectweb.asm.tree.MethodNode

fun MutableList<MethodNode>.removeMethods(vararg names: String) = removeIf { names.contains(it.name) }