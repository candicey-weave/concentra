package com.gitlab.candicey.concentra.transform.transformer

import com.gitlab.candicey.altera.alteration.Transformer
import com.gitlab.candicey.zenithcore.util.weave.named
import net.weavemc.internals.asm
import org.objectweb.asm.tree.ClassNode

object HudCoreTransformer : Transformer("cc/polyfrost/oneconfig/internal/hud/HudCore") {
    override fun transform(classNode: ClassNode) {
        with(classNode.methods.named("onInit").instructions) {
            clear()
            add(asm { _return })
        }
    }
}