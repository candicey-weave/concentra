package com.gitlab.candicey.concentra.transform.transformer

import com.gitlab.candicey.altera.alteration.Transformer
import com.gitlab.candicey.concentra.helper.GLPlatformImplHelper
import com.gitlab.candicey.zenithcore.util.weave.getSingleton
import com.gitlab.candicey.zenithcore.util.weave.internalNameOf
import com.gitlab.candicey.zenithcore.util.weave.named
import net.weavemc.internals.asm
import org.objectweb.asm.tree.ClassNode

object GLPlatformImplTransformer : Transformer("cc/polyfrost/oneconfig/platform/impl/GLPlatformImpl") {
    override fun transform(classNode: ClassNode) {
        val enableStencil = classNode.methods.named("enableStencil")
        val instructions = enableStencil.instructions

        instructions.clear()
        instructions.insert(asm {
            getSingleton<GLPlatformImplHelper>()
            invokevirtual(
                internalNameOf<GLPlatformImplHelper>(),
                "onEnableStencil",
                "()V"
            )
            _return
        })
    }
}