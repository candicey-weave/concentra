package com.gitlab.candicey.concentra.transform.fileeditor

import com.gitlab.candicey.altera.alteration.FileEditor

object MixinConfigFileEditor : FileEditor("mixins.oneconfig.json") {
    override fun edit(name: String, bytes: ByteArray): ByteArray {
        val string = String(bytes).trimIndent()

        val removeLines = listOf(
            "  \"plugin\": \"cc.polyfrost.oneconfig.internal.plugin.OneConfigMixinPlugin\",",
//            "  \"refmap\": \"mixins.oneconfig.refmap.json\",",
            "    \"GuiIngameForgeMixin\",",
            "    \"VigilantMixin\",",
        )

        return string.lines().filterNot { it in removeLines }.joinToString("\n").toByteArray()
    }
}