package com.gitlab.candicey.concentra.transform.transformer

import com.gitlab.candicey.altera.alteration.Transformer
import com.gitlab.candicey.concentra.extension.removeMethods
import com.gitlab.candicey.zenithcore.extension.addFieldSafe
import com.gitlab.candicey.zenithcore.util.weave.named
import org.objectweb.asm.Opcodes
import org.objectweb.asm.tree.*

object OneConfigTransformer : Transformer("cc/polyfrost/oneconfig/internal/OneConfig") {
    override fun transform(classNode: ClassNode) {
        classNode.addFieldSafe(
            FieldNode(
                Opcodes.ACC_PUBLIC + Opcodes.ACC_FINAL + Opcodes.ACC_STATIC,
                "TRANSFORMER_CONCENTRA_VERSION",
                "Ljava/lang/String;",
                null,
                "@@CONCENTRA_VERSION@@"
            )
        )

        classNode.methods.removeMethods("handleForgeCommand", "handleForgeGui", "lambda\$handleForgeGui\$2")

        val initMethodNode = classNode.methods.named("init")

        val tryCatchBlocks = initMethodNode.tryCatchBlocks

        tryCatchBlocks.removeAt(0)

        val instructions = initMethodNode.instructions

        val insnList = mutableListOf<AbstractInsnNode>()

        var found = false
        for (instruction in instructions) {
            if (!found) {
                if (instruction is MethodInsnNode && instruction.owner == "net/minecraftforge/fml/common/Loader") {
                    found = true
                }
            }

            if (found) {
                if (instruction is FieldInsnNode && instruction.opcode == Opcodes.GETSTATIC && instruction.owner == "cc/polyfrost/oneconfig/internal/config/compatibility/forge/ForgeCompat") {
                    break
                }

                insnList += instruction
            }
        }

        insnList.forEach(instructions::remove)
    }
}