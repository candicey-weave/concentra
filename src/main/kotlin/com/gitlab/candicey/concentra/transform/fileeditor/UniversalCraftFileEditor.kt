package com.gitlab.candicey.concentra.transform.fileeditor

import com.gitlab.candicey.altera.alteration.FileEditor
import org.objectweb.asm.ClassReader
import org.objectweb.asm.ClassWriter
import org.objectweb.asm.Opcodes
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.MethodInsnNode

object UniversalCraftFileEditor : FileEditor("cc/polyfrost/oneconfig/libs/universal/UScreen.class") {
    override fun edit(name: String, bytes: ByteArray): ByteArray {
        val classReader = ClassReader(bytes)
        val classNode = ClassNode()
        classReader.accept(classNode, 0)

        // replace super class with com.gitlab.candicey.zenithcore.child.GuiScreenChild
        val guiScreenChild = "com/gitlab/candicey/zenithcore_v${"@@ZENITH_CORE_VERSION@@".replace('.', '_')}/versioned/v1_8/child/GuiScreenChild"
        classNode.superName = guiScreenChild

        // boolean, int, String
        val constructor = classNode.methods.find { it.name == "<init>" && it.desc == "(ZILjava/lang/String;)V" }!!
        val instructions = constructor.instructions
        val initialiseSuperConstructor = instructions.find { it.opcode == Opcodes.INVOKESPECIAL } as MethodInsnNode
        initialiseSuperConstructor.owner = guiScreenChild

        val classWriter = ClassWriter(0)
        classNode.accept(classWriter)

        return classWriter.toByteArray()
    }
}