package com.gitlab.candicey.concentra.transform.transformer

import com.gitlab.candicey.altera.alteration.Transformer
import com.gitlab.candicey.concentra.wrapper.PlatformCommandManagerImplWrapper
import com.gitlab.candicey.zenithcore.util.weave.getSingleton
import com.gitlab.candicey.zenithcore.util.weave.internalNameOf
import com.gitlab.candicey.zenithcore.util.weave.named
import net.weavemc.internals.asm
import org.objectweb.asm.Opcodes
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.FieldInsnNode

object PlatformCommandManagerImplTransformer : Transformer("cc/polyfrost/oneconfig/utils/commands/PlatformCommandManagerImpl") {
    override fun transform(classNode: ClassNode) {
        val instructions = classNode.methods.named("createCommand").instructions

        val getClientCommandInstanceInsnNode = instructions.first { it.opcode == Opcodes.GETSTATIC } as FieldInsnNode
        val registerCommandInsnNode = instructions.first { it.opcode == Opcodes.INVOKEVIRTUAL }
        val pop = registerCommandInsnNode.next

        instructions.insert(getClientCommandInstanceInsnNode, asm {
            getSingleton<PlatformCommandManagerImplWrapper>()
        })
        instructions.remove(getClientCommandInstanceInsnNode)

        instructions.insert(registerCommandInsnNode, asm {
            invokevirtual(internalNameOf<PlatformCommandManagerImplWrapper>(), "registerCommand", "(Ljava/lang/Object;)V")
        })
        instructions.remove(registerCommandInsnNode)
        instructions.remove(pop)
    }
}