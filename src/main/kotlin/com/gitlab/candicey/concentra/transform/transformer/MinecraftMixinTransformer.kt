package com.gitlab.candicey.concentra.transform.transformer

import com.gitlab.candicey.altera.alteration.Transformer
import com.gitlab.candicey.concentra.extension.removeMethods
import org.objectweb.asm.tree.ClassNode

object MinecraftMixinTransformer : Transformer("cc/polyfrost/oneconfig/internal/mixin/MinecraftMixin") {
    override fun transform(classNode: ClassNode) {
        val methods = classNode.methods

        methods.removeMethods("onGuiOpenEvent")
    }
}