package com.gitlab.candicey.concentra.transform.finalbyteconsumer

object MixinRefMapRemover : (MutableMap<String, ByteArray>) -> Unit {
    override fun invoke(finalBytes: MutableMap<String, ByteArray>) {
        finalBytes.remove("mixins.oneconfig.refmap.json")
    }
}