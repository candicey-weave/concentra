package com.gitlab.candicey.concentra.transform.transformer

import com.gitlab.candicey.altera.alteration.Transformer
import com.gitlab.candicey.concentra.helper.LwjglManagerHelper
import com.gitlab.candicey.zenithcore.util.weave.getSingleton
import com.gitlab.candicey.zenithcore.util.weave.internalNameOf
import com.gitlab.candicey.zenithcore.util.weave.named
import net.weavemc.internals.asm
import org.objectweb.asm.Opcodes
import org.objectweb.asm.tree.*

const val LWJGL_MANAGER_IMPL = "cc/polyfrost/oneconfig/internal/renderer/LwjglManagerImpl"

object LwjglManagerImplTransformer : Transformer(LWJGL_MANAGER_IMPL) {
    override fun transform(classNode: ClassNode) {
        addGetOrDownloadJarFileMethod(classNode)

        transformConstructor(classNode)
        transformFindClass(classNode)
        transformDefineClassBypassMethod(classNode)
        transformGetJarFile(classNode)
    }

    private fun transformConstructor(classNode: ClassNode) {
        // GETSTATIC cc/polyfrost/oneconfig/internal/renderer/LwjglManagerImpl.jarFile Ljava/net/URL;
        // replace with INVOKESTATIC cc/polyfrost/oneconfig/internal/renderer/LwjglManagerImpl.getJarFile ()Ljava/net/URL;
        val methodNode = classNode.methods.named("<init>")
        val instructions = methodNode.instructions

        val jarFileInsnNode = instructions.first { it.opcode == Opcodes.GETSTATIC && (it as FieldInsnNode).name == "jarFile" }
        instructions.insertBefore(jarFileInsnNode, asm {
            invokestatic(
                LWJGL_MANAGER_IMPL,
                "getJarFile",
                "()Ljava/net/URL;"
            )
        })
        instructions.remove(jarFileInsnNode)
    }

    private fun transformFindClass(classNode: ClassNode) {
        val methodNode = classNode.methods.named("findClass")
        val instructions = methodNode.instructions

        /*
        * ALOAD remappedName (2)
        * ALOAD unmappedName (3)
        * INVOKEVIRTUAL java/lang/String.equals(Ljava/lang/Object;)Z
        * IFNE M
        */
        val insn1 = instructions
            .filterIsInstance<VarInsnNode>()
            .find { it1 ->
                it1.opcode == Opcodes.ALOAD && it1.`var` == 2
                        && it1.next.let { it2 ->
                    it2.opcode == Opcodes.ALOAD && (it2 as VarInsnNode).`var` == 3
                            && it2.next.let { it3 ->
                        it3.opcode == Opcodes.INVOKEVIRTUAL && (it3 as MethodInsnNode).name == "equals"
                                && it3.next.let { it4 ->
                            it4.opcode == Opcodes.IFNE
                        }
                    }
                }
            }
            ?: error("Cannot find ALOAD 2, ALOAD 3, INVOKEVIRTUAL java/lang/String.equals(Ljava/lang/Object;)Z, IFNE M")
        val insn2 = insn1.next
        val insn3 = insn2.next
        val insn4 = insn3.next

        instructions.remove(insn1)
        instructions.remove(insn2)
        instructions.remove(insn3)
        instructions.remove(insn4)

        /*
        * ALOAD classUrl (5)
        * INVOKESTATIC org/apache/commons/io/IOUtils.toByteArray(Ljava/net/URL;)[B
        */
        val classUrl = instructions
            .filterIsInstance<VarInsnNode>()
            .find {
                it.opcode == Opcodes.ALOAD && it.`var` == 5
                        && it.next.let { it1 ->
                    it1.opcode == Opcodes.INVOKESTATIC && (it1 as MethodInsnNode).name == "toByteArray"
                }
            } ?: error("Cannot find ALOAD 5, INVOKESTATIC org/apache/commons/io/IOUtils.toByteArray(Ljava/net/URL;)[B")
        val classUrlNext = classUrl.next
        instructions.insert(classUrl, asm {
            getSingleton<LwjglManagerHelper>()
            aload(0)
            aload(4)
            invokevirtual(internalNameOf<LwjglManagerHelper>(), "getByteArray", "(Ljava/net/URLClassLoader;Ljava/lang/String;)[B")
        })
        instructions.remove(classUrl)
        instructions.remove(classUrlNext)
    }

    private fun transformDefineClassBypassMethod(classNode: ClassNode) {
        val methodNode = classNode.methods.named("defineClassBypass")
        val instructions = methodNode.instructions

        val newRemapperClassAdapter =
            instructions.find { it.opcode == Opcodes.NEW && (it as TypeInsnNode).desc == "org/objectweb/asm/commons/RemappingClassAdapter" }!!
        val accept =
            instructions.find { it.opcode == Opcodes.INVOKEVIRTUAL && (it as MethodInsnNode).name == "accept" }!!

        // remove from newRemapperClassAdapter to accept
        var inProgress = false

        for (instruction in instructions) {
            if (instruction == newRemapperClassAdapter) {
                inProgress = true
            }

            if (inProgress) {
                instructions.remove(instruction)
            }

            if (instruction == accept) {
                break
            }
        }
    }

    private fun transformGetJarFile(classNode: ClassNode) {
        val methodNode = classNode.methods.named("getJarFile")
        val instructions = methodNode.instructions

        methodNode.localVariables.clear()
        methodNode.tryCatchBlocks.clear()

        instructions.clear()
        instructions.insert(asm {
            getSingleton<LwjglManagerHelper>()
            invokevirtual(
                internalNameOf<LwjglManagerHelper>(),
                "getJarFile",
                "()Ljava/net/URL;"
            )
            areturn
        })
    }

    private fun addGetOrDownloadJarFileMethod(classNode: ClassNode) {
        classNode.methods.add(
            MethodNode(
                Opcodes.ACC_PUBLIC + Opcodes.ACC_STATIC,
                "getOrDownloadJarFile",
                "()Ljava/net/URL;",
                null,
                null
            ).apply {
                instructions.insert(asm {
                    getSingleton<LwjglManagerHelper>()
                    invokevirtual(
                        internalNameOf<LwjglManagerHelper>(),
                        "getOrDownloadJarFile",
                        "()Ljava/io/File;"
                    )
                    invokevirtual("java/io/File", "toURI", "()Ljava/net/URI;")
                    invokevirtual("java/net/URI", "toURL", "()Ljava/net/URL;")
                    areturn
                })
            }
        )
    }
}