package com.gitlab.candicey.concentra.helper

import com.gitlab.candicey.concentra.configDirectory
import com.gitlab.candicey.concentra.debug
import com.gitlab.candicey.concentra.info
import com.gitlab.candicey.concentra.transform.transformer.LWJGL_MANAGER_IMPL
import com.gitlab.candicey.concentra.warn
import com.gitlab.candicey.zenithcore.extension.sha256
import com.gitlab.candicey.zenithcore.util.reflectField
import com.gitlab.candicey.zenithcore.util.reflectMethod
import com.gitlab.candicey.zenithcore.util.runAsync
import org.objectweb.asm.tree.ClassNode
import java.io.File
import java.io.IOException
import java.lang.reflect.Method
import java.net.MalformedURLException
import java.net.URL
import java.net.URLClassLoader
import java.nio.file.Files
import java.nio.file.StandardCopyOption
import java.util.jar.JarFile

object LwjglManagerHelper {
    private const val POLYFROST_LEGACY_LWJGL_MAVEN_JAR = "https://repo.polyfrost.cc/releases/cc/polyfrost/lwjgl-legacy/1.0.0-alpha26/lwjgl-legacy-1.0.0-alpha26.jar"
    private const val POLYFROST_LEGACY_LWJGL_MAVEN_JAR_SHA256SUM = "1dd73a0b9448468ebaa4b18157d1dab019249f91c837e0cbeab52519dad2b130"

    private const val JAR_NAME = "concentra-lwjgl3.jar"
    private var jarFile: File? = null
    private var entries: Map<String, ByteArray>? = null

    private val unsafeInstance by lazy { reflectField(LWJGL_MANAGER_IMPL, "unsafeInstance")[null] }
    private val defineClassMethod by lazy { reflectField(LWJGL_MANAGER_IMPL, "defineClassMethod")[null] as Method }
    private val remappingMap by lazy { reflectField(LWJGL_MANAGER_IMPL, "remappingMap")[null] as Map<String, String> }
    private val transformMethod by lazy { reflectMethod(LWJGL_MANAGER_IMPL, "transform", ClassNode::class.java) }

    fun getJarFile(): URL {
        while (jarFile == null) {
            try {
                info("Waiting for LWJGL jar to download...")
                Thread.sleep(100)
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
        }

        return jarFile!!.toURI().toURL()
    }

    private fun getOrDownloadJarFile(): File {
        val tempJar = File(configDirectory, "cache/$JAR_NAME")
        try {
            tempJar.parentFile.mkdirs()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            tempJar.createNewFile()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        if (tempJar.sha256 == POLYFROST_LEGACY_LWJGL_MAVEN_JAR_SHA256SUM) {
            return try {
                tempJar
            } catch (e: MalformedURLException) {
                throw RuntimeException(e)
            }
        }
        info("Downloading LWJGL jar from Polyfrost... ($POLYFROST_LEGACY_LWJGL_MAVEN_JAR)")

        URL(POLYFROST_LEGACY_LWJGL_MAVEN_JAR).openStream().use { inputStream ->
            Files.copy(inputStream, tempJar.toPath(), StandardCopyOption.REPLACE_EXISTING)
            debug("Copied LWJGL jar to " + tempJar.absolutePath)
        }

        val sha256sum = tempJar.sha256
        if (sha256sum != POLYFROST_LEGACY_LWJGL_MAVEN_JAR_SHA256SUM) {
            warn("LWJGL jar checksum mismatch! Expected: $POLYFROST_LEGACY_LWJGL_MAVEN_JAR_SHA256SUM, got: $sha256sum")
            return getOrDownloadJarFile()
        }

        return try {
            tempJar
        } catch (e: MalformedURLException) {
            throw RuntimeException(e)
        }
    }

    fun getByteArray(instance: URLClassLoader, path: String): ByteArray =
        (entries ?: error("entries is null"))[path.replace('.', '/') + ".class"]
            ?: instance.getResourceAsStream("/${path.replace('.', '/')}.class").use { it?.readBytes() }
            ?: javaClass.getResourceAsStream("/${path.replace('.', '/')}.class").use { it?.readBytes() }
            ?: error("entries[$path] is null")

    fun initStatic() {
        runAsync {
            jarFile = getOrDownloadJarFile()
            entries = JarFile(jarFile).use { jar ->
                val entries = jar.entries().asSequence().map { it.name }.toSet()
                entries.associateWith { jar.getInputStream(jar.getEntry(it)).readBytes() }
            }
        }
    }
}