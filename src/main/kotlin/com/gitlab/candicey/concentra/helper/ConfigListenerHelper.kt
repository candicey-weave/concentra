package com.gitlab.candicey.concentra.helper

import com.gitlab.candicey.zenithcore.util.reflectField

object ConfigListenerHelper {
    fun onAddListener(config: Any, optionName: String, listener: Runnable) {
        if (optionName == "enabled") {
            val enabledListeners = config.reflectField("enabledListeners")[config] as ArrayList<Runnable>
            enabledListeners.add(listener)
        }
    }

    fun onSetEnabled(modCard: Any) {
        val mod = modCard.reflectField("modData")[modCard]!!
        val config = mod.reflectField("config")[mod]!!
        val enabledListeners = config.reflectField("enabledListeners")[config] as ArrayList<Runnable>
        enabledListeners.forEach(Runnable::run)
    }
}