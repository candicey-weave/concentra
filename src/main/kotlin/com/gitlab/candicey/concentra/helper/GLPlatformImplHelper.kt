package com.gitlab.candicey.concentra.helper

import com.gitlab.candicey.concentra.extension.concentraStencilEnabled
import com.gitlab.candicey.concentra.extension.enableStencil
import com.gitlab.candicey.zenithcore.versioned.v1_8.mc

object GLPlatformImplHelper {
    fun onEnableStencil() {
        val frameBuffer = mc.framebuffer

        if (frameBuffer.concentraStencilEnabled) {
            frameBuffer.enableStencil()
        }
    }
}